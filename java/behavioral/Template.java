package behavioral;

abstract class Computer {
    protected abstract void motherboard();
    protected abstract void cpu();
    protected abstract void powerSupply();
    protected abstract void graphicsCard();
    protected abstract void hdd();
    public final void makeCar(){
        motherboard();
        cpu();
        powerSupply();
        graphicsCard();
        hdd();
    }
}

class officeComputer extends Computer {
    public void motherboard(){
        System.out.println("ASUS PRIME H310M-R R2.0");
    }
    public void cpu(){
        System.out.println("Intel Pentium Gold G5420");
    }

    public void powerSupply(){
        System.out.println("AEROCOOL VX PLUS 400W");
    }

    public void graphicsCard(){
        System.out.println("Intel UHD Graphics 610");
    }

    public void hdd(){
        System.out.println("500Gb");
    }
}

class gameComputer extends Computer {

    public void motherboard(){
        System.out.println("Huananzhi x99-8m-f gaming");
    }

    public void cpu(){
        System.out.println("Intel xeon e5 2620 v3");
    }

    public void powerSupply(){
        System.out.println("Cougar XTC600");
    }

    public void graphicsCard(){
        System.out.println("Nvidia GeForce GTX1660 SUPER");
    }

    public void hdd(){
        System.out.println("1Tb");
    }
}