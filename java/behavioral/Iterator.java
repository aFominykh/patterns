package behavioral;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

interface Iterator {
    boolean hasNext();
    Object next();
    void reset();
}

interface Collection {
    Iterator getIterator();
}

class DocumentFolder implements Collection{
    private List<String> documents;

    public DocumentFolder(){
        this.documents = new ArrayList<>();
        Collections.addAll(documents, "order", "contract", "shares", "decree");
    }

    public DocumentFolder(List<String> documents){
        this.documents = new ArrayList<>();
        this.documents.addAll(documents);
    }

    public Iterator getIterator() {
        return new DocumentIterator();
    }

    private class DocumentIterator implements Iterator{
        private int index = 0;

        public boolean hasNext() {
            return index < documents.size();
        }

        public Object next() {
            return documents.get(index++);
        }

        public void reset() {
            this.index = 0;
        }
    }
}