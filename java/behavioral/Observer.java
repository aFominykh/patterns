package behavioral;

import java.util.ArrayList;
import java.util.List;

interface Observer {
    void update(boolean tv, boolean internet, boolean videcam);
}

interface Observable {
    void subscribeObserver(Observer o);
    void unsubscribeObserver(Observer o);
    void notifyObservers();
}

class Rostelecom implements Observable{
    private List<Observer> subscribers = new ArrayList<>();
    private boolean tv;
    private boolean internet;
    private boolean videcam;

    public Rostelecom() {
        subscribers = new ArrayList<>();
    }

    public List<Observer> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<Observer> subscribers) {
        this.subscribers = subscribers;
    }

    public boolean isTv() {
        return tv;
    }

    public void setTv(boolean tv) {
        this.tv = tv;
    }

    public boolean isInternet() {
        return internet;
    }

    public void setInternet(boolean internet) {
        this.internet = internet;
    }

    public boolean isVidecam() {
        return videcam;
    }

    public void setVidecam(boolean videcam) {
        this.videcam = videcam;
    }

    public void subscribeObserver(Observer o) {
        subscribers.add(o);
    }

    public void unsubscribeObserver(Observer o) {
        subscribers.remove(o);
    }

    public void notifyObservers() {
        for (Observer observer : subscribers) {
            observer.update(tv, internet, videcam);
        }
    }
}

class Subscriber implements Observer {
    private boolean tv;
    private boolean internet;
    private boolean videcam;
    private Rostelecom rostelecom;

    public Subscriber(Rostelecom rostelecom){
        this.rostelecom = rostelecom;
        rostelecom.subscribeObserver(this);
    }

    public void update(boolean tv, boolean internet, boolean videcam) {
        this.tv = tv;
        this.internet = internet;
        this.videcam = videcam;
        showSubscriptions();
    }

    public void showSubscriptions() {
        System.out.println("Tv: " + tv + " Internet: " + internet + " Videcam: " + videcam);
    }
}



