package generating;

enum Genre {
    FANTASY,
    HISTORICAL,
    HORROR,
    THRILLER;
}

enum Language {
    RUSSIAN,
    ENGLISH;
}

enum Budget {
    SMALL,
    NORMAL,
    BIG;
}

interface Builder {
    void setGenre(Genre genre);
    void setTime(int time);
    void setBudget(Budget budget);
    void setLanguage(Language language);
    void setSubtitles(String subtitles);
}

class Movie {
    private Genre genre;
    private int time;
    private Budget budget;
    private Language language;
    private String subtitles;

    public Movie(Genre genre, int time, Budget budget, Language language, String subtitles) {
        if(genre != null && budget != null && language != null) {
            this.genre = genre;
            this.time = time;
            this.budget = budget;
            this.language = language;
            this.subtitles = subtitles;
        }
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getSubtitles() {
        return subtitles;
    }

    public void setSubtitles(String subtitles) {
        this.subtitles = subtitles;
    }
}

class GameBuilder implements Builder{
    private Genre genre;
    private int time;
    private Budget budget;
    private Language language;
    private String subtitles;

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public void setSubtitles(String subtitles) {
        this.subtitles = subtitles;
    }

    public Movie getResult(){
        return new Movie(genre, time, budget, language, subtitles);
    }
}

class Director {
    public void constructShooter(Builder builder){
        builder.setGenre(Genre.THRILLER);
        builder.setTime(208);
        builder.setBudget(Budget.NORMAL);
        builder.setLanguage(Language.RUSSIAN);
        builder.setSubtitles("Created by *** team");
    }

    public void constructPlatformer(Builder builder){
        builder.setGenre(Genre.HISTORICAL);
        builder.setTime(120);
        builder.setBudget(Budget.SMALL);
        builder.setLanguage(Language.ENGLISH);
        builder.setSubtitles("Created by *** team");
    }
}