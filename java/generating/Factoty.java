package generating;

interface ISort {
    void sort(int [] arr);
}

class CountingSort implements ISort {
    public void sort(int [] arr){
        int length = arr.length;
        int [] c = new int[length+1];
        for (int i = 0; i < length; i++) {
            c[arr[i]] = c[arr[i]] + 1;
        }
        int b = 0;
        for (int i = 0; i < length+1; i++){
            for (int j = 0; j < c[i]; j++) {
                arr[b] = i;
                b = b + 1;
            }
        }
    }

}

class InsertionSort implements ISort {
    public void sort(int [] arr){
        int length = arr.length;
        for (int i = 1; i < length; ++i) {
            int key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }
}

class MergeSort implements ISort {
    public void sort(int [] arr){
        mergeSort(arr,arr.length);
    }
    public static void mergeSort(int[] a, int length) {
        if (length < 2) {
            return;
        }
        int mid = length / 2;
        int[] l = new int[mid];
        int[] r = new int[length - mid];
        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < length; i++) {
            r[i - mid] = a[i];
        }
        mergeSort(l, mid);
        mergeSort(r, length - mid);
        merge(a, l, r, mid, length - mid);
    }

    public static void merge(int[] a, int[] l, int[] r, int left, int right) {
        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            }
            else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }
}

class SortFactory {

    public enum SortTypes{
        COUNTING,
        INSERTION,
        MERGE
    }

    public static ISort createSort(SortTypes sortType){
        ISort sort = null;
        switch (sortType) {
            case COUNTING:
                sort = new CountingSort();
                break;
            case INSERTION:
                sort = new InsertionSort();
                break;
            case MERGE:
                sort = new MergeSort();
                break;
        }
        return sort;
    }

}

class Sort {
    public final SortFactory sortFactory;
    public Sort(SortFactory sortFactory) {
        this.sortFactory = sortFactory;
    }
    public void sort(SortFactory.SortTypes chosenSortType, int[] arr){
        ISort sortType = SortFactory.createSort(chosenSortType);
        sortType.sort(arr);
    }
}
