package generating;

interface Sedan {
    String type();
}

class ToyotaSedan implements Sedan {
    public String type() {
        return "Toyota";
    }
}

class SubaruSedan implements Sedan {
    public String type() {
        return "Subaru";
    }
}

interface StationWagon {
    String type();
}

class ToyotaStationWagon implements StationWagon {
    public String type() {
        return "Toyota";
    }
}

class SubaruStationWagon implements StationWagon {
    public String type() {
        return "Subaru";
    }
}

interface GUIFactory {
    Sedan createSedan();
    StationWagon createStationWagon();
}

class ToyotaFactory implements GUIFactory {
    public Sedan createSedan() {
        return new ToyotaSedan();
    }

    public StationWagon createStationWagon() {
        return new ToyotaStationWagon();
    }
}

class SubaruFactory implements GUIFactory {
    public Sedan createSedan() {
        return new SubaruSedan();
    }

    public StationWagon createStationWagon() {
        return new SubaruStationWagon();
    }
}

class Application {
    private Sedan sedan;
    private StationWagon stationWagon;

    public Application(GUIFactory factory){
        sedan = factory.createSedan();
        stationWagon = factory.createStationWagon();
    }

    public String[] Created(){
        return new String[]{sedan.type(), stationWagon.type()};
    }

}