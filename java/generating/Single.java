package generating;

import java.util.ArrayList;

class SchoolDatabase {
    private static SchoolDatabase instance;
    int[] marks;
    private ArrayList<String> student;
    private SchoolDatabase(int[] mark, ArrayList<String> student){
        this.marks = mark;
        this.student = student;
    }
    public static SchoolDatabase getInstance(int[] mark, ArrayList<String> student){
        if(instance==null){
            instance = new SchoolDatabase(mark, student);
        }
        return instance;
    }
}