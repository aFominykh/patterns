package structural;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class User {
    private String login;
    private String password;
    private String firstName;
    private String lastName;

    public User(String login, String password, String firstName, String lastName) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

class MessageResponse {
    private Map<String, ArrayList<Object>> log;

    public MessageResponse(){
        log = new HashMap<>();
    }

    public void messageConnection(User user, String message){
        if(!log.containsKey(user.getLogin())){
            log.put(user.getLogin(), new ArrayList<>());
        }
        log.get(user.getLogin()).add(message);
    }

    public List<Object> getMessages(User user){
        if(log.containsKey(user.getLogin())){
            return log.get(user.getLogin());
        }
        return null;
    }

}

class MessageRequest {

    public MessageRequest(){ }

    public void messageConnection(User user1, User user2, String message, MessageResponse log){
        log.messageConnection(user2, message + " " + "Sender: " + user1.getLogin());
    }

}


class Facade {
    private MessageResponse log;
    private MessageRequest send;

    public Facade(){
        log = new MessageResponse();
        send = new MessageRequest();
    }

    public void sendMessage(User user1, User user2, String message){
        this.send.messageConnection(user1, user2, message, log);
    }

    public List<Object> getUserMessages(User user){
        return log.getMessages(user);
    }
}
