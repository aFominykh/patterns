package structural;

class HardDrive {
    private double amperage;
    private double resistance;

    public HardDrive(double amperage, double resistance){
        this.amperage=amperage;
        this.resistance=resistance;
    }

    public double getAmperage() {
        return amperage;
    }

    public void setAmperage(double amperage) {
        this.amperage = amperage;
    }

    public double getResistance() {
        return resistance;
    }

    public void setResistance(double resistance) {
        this.resistance = resistance;
    }
}

class Mouse {
    private double voltage;

    public Mouse(double voltage){
        this.voltage=voltage;
    }

    public Mouse(){}

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }
}

class USBAdapter extends Mouse {
    private HardDrive hardDrive;

    public USBAdapter(HardDrive hardDrive) {
        this.hardDrive = hardDrive;
    }

    public double getVoltage() {
        double result;
        result = hardDrive.getAmperage()*hardDrive.getResistance();
        return result;
    }
}

class USBport {
    private double voltage;

    public USBport(double voltage){
        this.voltage=voltage;
    }

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

    public boolean isSafeToUse(Mouse mouse){
        return this.getVoltage()>= mouse.getVoltage();
    }
}