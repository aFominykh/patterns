package structural;

import java.util.ArrayList;
import java.util.List;

interface Bag {
    double getColor();
    double getSize();
    List<String> getContent();

}

class Purse implements Bag {
    private double color;
    private double size;
    private List<String> content;
    public Purse(double color, double size, ArrayList<String> content){
        this.color = color;
        this.size = size;
        this.content = content;
    }

    public double getColor() {
        return color;
    }

    public void setColor(double color) {
        this.color = color;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }
}

class PencilCase implements Bag {
    private double color;
    private double size;
    private List<String> content;

    public PencilCase(double color, double size, ArrayList<String> content) {
        this.color = color;
        this.size = size;
        this.content = content;
    }

    public double getColor() {
        return color;
    }

    public void setColor(double color) {
        this.color = color;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }
}


class Order implements Bag {
    private List<Bag> order;
    private double color;
    private double size;

    public Order(List<Bag> order) {
        this.order = order;
        for (Bag bag : order) {
            size += bag.getSize();
            color += bag.getColor();
        }
    }

    public void orderAdd(Bag bag) {
        order.add(bag);
    }

    public void remove(Bag bag) {
        if (order != null) {
            order.remove(bag);
        }
    }

    public double getSize() {
        return size;
    }

    public double getColor() {
        return color;
    }

    public List<String> getContent() {
        List<String> result = new ArrayList<>();
        for (Bag bag : order) {
            result.addAll(bag.getContent());
        }
        return result;
    }
}
